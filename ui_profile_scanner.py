import PyQt5
from PyQt5 import QtWidgets, QtGui
import pyqtgraph as pg


class uiProfileScanner(object):
    def __init__(self, widget):
        main_layout = QtWidgets.QGridLayout(widget)

        self.image_item = pg.ImageItem()

        self.image_plot = pg.PlotWidget(widget)
        # self.image_plot.hideAxis('bottom')
        # self.image_plot.hideAxis('left')

        self.image_plot.addItem(self.image_item)
        self.h_line = self.image_plot.addLine(y=0, movable=True)
        self.v_line = self.image_plot.addLine(x=0, movable=True)


        self.h_profile_plot = pg.PlotWidget(widget)
        self.h_profile_plot.disableAutoRange()
        self.v_profile_plot = pg.PlotWidget(widget)

        main_layout.setColumnStretch(0, 2)
        main_layout.setRowStretch(0, 2)
        main_layout.setColumnStretch(1, 1)
        main_layout.setRowStretch(1, 1)

        main_layout.addWidget(self.image_plot, 0, 0)


        main_layout.addWidget(self.h_profile_plot, 1, 0)
        main_layout.addWidget(self.v_profile_plot, 0, 1)


if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    widget = QtWidgets.QWidget()
    widget.resize(500, 500)
    ui = uiProfileScanner(widget)
    widget.show()
    sys.exit(app.exec_())
