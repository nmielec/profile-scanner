import numpy as np
from PyQt5 import QtWidgets, QtCore

from ui_profile_scanner import uiProfileScanner

class ProfileScannerWidget(QtWidgets.QWidget):
    def __init__(self, parent=None, image=None, **kwargs):
        super().__init__(parent=parent, **kwargs)

        self.ui = uiProfileScanner(self)
        self.resize(700, 700)

        if image is not None:
            self.set_image(image)

        self.set_connections()

        self.plot_h_profile(0)
        self.plot_v_profile(0)

    def set_image(self, image):
        self.image = image
        self.ui.image_item.setImage(image)
        width, height = image.shape
        self.ui.image_plot.setLimits(xMin=-5, xMax=width + 5, yMin=-5, yMax=height + 5)

        self.ui.h_line.setBounds((0, height - 0.1))
        self.ui.v_line.setBounds((0, width - 0.1))

        self.reset_profile_plots_ranges()


    def reset_profile_plots_ranges(self):
        max_value = self.image.max()
        self.ui.h_profile_plot.setXRange(0, self.image.shape[0])
        self.ui.h_profile_plot.setYRange(0, max_value)
        self.ui.v_profile_plot.setYRange(0, self.image.shape[0])
        self.ui.v_profile_plot.setXRange(0, max_value)


    def set_connections(self):
        self.ui.h_line.sigPositionChanged.connect(self.on_h_line_moved)
        self.ui.v_line.sigPositionChanged.connect(self.on_v_line_moved)
        self.ui.image_plot.sigYRangeChanged.connect(self.on_image_plot_range_changed)
        self.ui.image_plot.sigXRangeChanged.connect(self.on_image_plot_range_changed)

        self.ui.image_plot.sceneObj.sigMouseClicked.connect(self.on_mouse_click)

    def on_mouse_click(self, mouse_event):
        if mouse_event.button() != QtCore.Qt.LeftButton:
            return
        pos = self.ui.image_plot.plotItem.vb.mapSceneToView(mouse_event.scenePos())
        x, y = pos.x(), pos.y()

        self.ui.h_line.setValue(y)
        self.ui.v_line.setValue(x)
        mouse_event.accept()

    def on_image_plot_range_changed(self):
        self.ui.v_profile_plot.setYRange(*self.ui.image_plot.viewRange()[1])
        self.ui.h_profile_plot.setXRange(*self.ui.image_plot.viewRange()[0])

    def on_h_line_moved(self):
        self.plot_h_profile(int(self.ui.h_line.value()))

    def plot_h_profile(self, row):
        xdata = np.arange(self.image.shape[0])
        ydata = self.image[:, row]

        self.ui.h_profile_plot.clear()
        self.ui.h_profile_plot.plot(xdata, ydata)


    def on_v_line_moved(self):
        self.plot_v_profile(int(self.ui.v_line.value()))

    def plot_v_profile(self, col):
        xdata = np.arange(self.image.shape[1])
        ydata = self.image[col, :]

        self.ui.v_profile_plot.clear()
        self.ui.v_profile_plot.plot(ydata, xdata)



if __name__ == '__main__':
    import numpy as np
    import scipy.misc

    np.random.seed(0)
    n = 2**7
    x, y = np.meshgrid(np.arange(n), np.arange(2*n))
    r = np.sqrt((x-n//2)**2 + (y-n)**2)
    image = np.exp(-r**2/20**2) + np.random.random((2*n, n))*0.2

    # filename = r"Z:\Cavity_project\top_hat\data\2018\2018_02_14\ccd scans\ccd scan 0 cm\scan2.png"
    filename = r"Z:\Cavity_project\top_hat\data\2018\2018_02_14\new afocal\2018_02_14-103616 - 0 cm.png"
    filename = r"Z:\Cavity_project\top_hat\data\2018\2018_02_14\ccd scans\ccd scan 45cm\scan.png"

    image = scipy.misc.imread(filename)
    if len(image.shape) > 2:
        image = image[:, :, 0]


    app = QtWidgets.QApplication([])
    widget = ProfileScannerWidget(image=image)
    widget.show()
    app.exec_()
